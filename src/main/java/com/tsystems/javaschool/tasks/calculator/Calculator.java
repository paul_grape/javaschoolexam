package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

  /**
   * Evaluate statement represented as string.
   *
   * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
   *                  parentheses, operations signs '+', '-', '*', '/'<br>
   *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
   * @return string value containing result of evaluation or null if statement is invalid
   */

  private class ExpressionParser {
    private String operators = "+-*/";
    private String delimiters = "() " + operators;

    private boolean isDelimiter(String token) {
      if (token.length() != 1) {
        return false;
      }
      for (int i = 0; i < delimiters.length(); i++) {
        if (token.charAt(0) == delimiters.charAt(i)) {
          return true;
        }
      }
      return false;
    }

    private boolean isOperator(String token) {
      for (int i = 0; i < operators.length(); i++) {
        if (!token.isEmpty()) {
          if (token.charAt(0) == operators.charAt(i)) {
            return true;
          }
        } else {
          return false;
        }
      }
      return false;
    }

    private int priority(String token) {
      if (token.equals("(")) return 1;
      if (token.equals("+") || token.equals("-")) return 2;
      if (token.equals("*") || token.equals("/")) return 3;
      return 4;
    }

    private List<String> parse(String infix) { // postfix notation
      List<String> postfix = new ArrayList<>();
      Deque<String> stack = new ArrayDeque<>();
      StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true); //parse expression with delimeters
      String prev = ""; //previous token of expression
      String curr = ""; //current token of expression
      if (!infix.isEmpty()) {
        while (tokenizer.hasMoreTokens()) {
          curr = tokenizer.nextToken();
          if (isOperator(curr) && isOperator(prev)) {
            return null;
          }
          if (!tokenizer.hasMoreTokens() && isOperator(curr)) { //incorrect expression
            return null;
          }
          if (curr.equals(" ")) {
            continue;
          } else if (isDelimiter(curr)) {
            if (curr.equals("(")) {
              stack.push(curr);
            } else if (curr.equals(")")) {
              while (!stack.peek().equals("(")) {
                postfix.add(stack.pop());
                if (stack.isEmpty()) { //parentheses don't match
                  return null;
                }
              }
              stack.pop();
              if (!stack.isEmpty()) {
                postfix.add(stack.pop());
              }
            } else {
              while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                postfix.add(stack.pop());
              }
              stack.push(curr);
            }
          } else {
            try {
              Double.valueOf(curr); // exception if wrong double delimeter
              postfix.add(curr);
            } catch (Exception e) {
              return null;
            }
          }
          prev = curr;
        }
      } else {
        return null;
      }
      while (!stack.isEmpty()) {
        if (isOperator(stack.peek())) {
          postfix.add(stack.pop());
        } else {
          return null; // parentheses don't match
        }
      }
      return postfix;
    }
  }

  private static Double calc(List<String> postfix) {
    Deque<Double> stack = new ArrayDeque<>();
    for (String x : postfix) {
      if (x.equals("+")) {
        stack.push(stack.pop() + stack.pop());
      } else if (x.equals("-")) {
        Double b = stack.pop(), a = stack.pop();
        stack.push(a - b);
      } else if (x.equals("*")) {
        stack.push(stack.pop() * stack.pop());
      } else if (x.equals("/")) {
        Double b = stack.pop(), a = stack.pop();
        Double res = a / b;
        if (!res.isInfinite()) { // divide by zero
          stack.push(a / b);
        } else {
          return null;
        }
      } else {
        stack.push(Double.valueOf(x));
      }
    }
    return stack.pop();
  }

  public String evaluate(String statement) {
    if (statement == null) {
      return null;
    }
    ExpressionParser expressionParser = new ExpressionParser();
    List<String> expression = expressionParser.parse(statement);
    if (expression == null) {
      return null;
    }
    Double res = calc(expression);
    if (res == null) {
      return null;
    }
    if (res.intValue() == res) {
      return Integer.toString(res.intValue());
    }
    return (res.toString());
  }
}
