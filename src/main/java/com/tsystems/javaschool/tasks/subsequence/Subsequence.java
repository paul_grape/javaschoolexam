package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

  /**
   * Checks if it is possible to get a sequence which is equal to the first
   * one by removing some elements from the second one.
   *
   * @param x first sequence
   * @param y second sequence
   * @return <code>true</code> if possible, otherwise <code>false</code>
   */
  @SuppressWarnings("rawtypes")
  public boolean find(List x, List y) {
    if (x == null || y == null) {
      throw new IllegalArgumentException();
    }

    if (y.size() < x.size()) {
      return false;
    }

    int count = 0; // number of coincidences
    int begPoint = 0; // to start from begPoint index on every iteration
    for (int i = 0; i < x.size(); ++i) {
      for (int j = begPoint; j < y.size(); ++j) {
        if (x.get(i).equals(y.get(j))) {
          begPoint = j;
          ++count;
          break;
        }
      }
    }
    return count == x.size();
  }
}
