package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

  /**
   * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
   * from left to right). All vacant positions in the array are zeros.
   *
   * @param inputNumbers to be used in the pyramid
   * @return 2d array with pyramid inside
   * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
   */
  public int[][] buildPyramid(List<Integer> inputNumbers) {

    int listSize = inputNumbers.size();
    int rowNumber = 0;
    while (listSize > 0) {
      listSize = inputNumbers.size();
      ++rowNumber;
      listSize -= rowNumber * (rowNumber + 1) / 2;
    }

    if (listSize < 0) {
      throw new CannotBuildPyramidException();
    } else {
      int[][] result = new int[rowNumber][2 * rowNumber - 1];

      try {
        Collections.sort(inputNumbers);
      } catch (Exception e) { // if NaN is in List
        throw new CannotBuildPyramidException();
      }

      int rightBorder = 2 * rowNumber - 1; // last element of row
      int leftBorder = 0; // first element of row
      int offset = 0; // offset for list

      for (int i = 0; i < rowNumber; ++i) { // initialize matrix with zeros
        for (int j = 0; j < 2 * rowNumber - 1; ++j) {
          result[i][j] = 0;
        }
      }

      for (int i = rowNumber - 1; i >= 0; --i) {
        for (int j = rightBorder - 1; j >= leftBorder; --j) {
          if ((j % 2 == 0 && rightBorder % 2 == 1) || (j % 2 == 1 && rightBorder % 2 == 0)) {
            ++offset;
            result[i][j] = inputNumbers.get(inputNumbers.size() - offset);
          }
        }
        --rightBorder;
        ++leftBorder;
      }
      return result;
    }
  }
}
